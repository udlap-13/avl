//Clase del arbol AVL, la cual se encarga de crear, borrar o buscar Nodos y de conectarlos entre si apropiadamente segun la estructura AVL
public class ArbolAVL {
	Nodo raiz;
	
	public ArbolAVL() {
		raiz=null;
	}
	
	//Este metodo se encarga de agregar los datos dados por el usuario en la posicion correspondiente
	public boolean agregar(int dato){
		Nodo temp;
		
		if(raiz==null){
			raiz=new Nodo(dato, null);
			return true;
		}
		else{
			temp=raiz;
			while(temp!=null){
				if (dato==temp.dato)
					return false;
				if((dato>temp.dato)&&(temp.der==null)){
					temp.der = new Nodo(dato, temp);
					balanceAdd(temp, 1);
					return true;
				}

				else if ((dato<temp.dato)&&(temp.izq==null)){
					temp.izq = new Nodo(dato, temp);
					balanceAdd(temp,-1);
					return true;
				}

				else if(dato>temp.dato)
					temp=temp.der;
				
				else
					temp=temp.izq;
			}
			return false;
		}
	}
	
	//Este metodo sirve para simplificar el input necesario para el m�todo principal de eliminar
	//Y de esta forma asegurarse de eliminar el dato desde la raiz
	public boolean eliminar(int dato){
		return eliminar(raiz, dato);
	}
	
	//Metodo encargado de elminar un dato del �rbol, buscandolo a partir del nodo especificado
	public boolean eliminar(Nodo n, int dato) {
		Nodo temp = buscar(n, dato);
		
		if(temp != null) {
			//Caso donde nodo a eliminiar es hoja
			if((temp.izq==null)&&(temp.der==null)){
				if(temp==raiz)
					raiz=null;
				else {
					if(temp.padre.dato>=temp.dato) {
						temp.padre.izq=null;
						balanceDel(temp.padre,-1);
					}
					else {
						temp.padre.der=null;
						balanceDel(temp.padre,1);
					}
					temp=null;
				}
			}
			//Caso donde nodo tiene un descendiente izquierdo
			else if((temp.izq!=null)&&(temp.der==null)) {
				if(temp!=raiz) {		  
					temp.izq.padre = temp.padre;
					
					if(temp.padre.dato>=temp.dato) {
						temp.padre.izq = temp.izq;
						balanceDel(temp.padre,-1);
					}
					else {
						temp.padre.der=temp.izq;
						balanceDel(temp.padre,1);
					}
				}
				else {
					raiz = temp.izq;
					raiz.padre=null;
					balanceDel(raiz,0);
				}
			}
			//Nodo con un descendiente derecho
			else if((temp.izq==null)&&(temp.der!=null)){
				if(temp!=raiz) {		  
					temp.der.padre = temp.padre;
					
					if(temp.padre.dato>=temp.dato) {
						temp.padre.izq = temp.der;
						balanceDel(temp.padre,-1);
					}
					else {
						temp.padre.der=temp.der;
						balanceDel(temp.padre,1);
					}
				}
				else {
					raiz = temp.der;
					raiz.padre = null;
					balanceDel(raiz, 0);
				}
			}
			//Nodo con ambos descendientes
			else {
				Nodo busqueda = temp.izq;
				while(busqueda.der != null){
					busqueda = busqueda.der;
				}
				
				temp.dato = busqueda.dato;
				
				eliminar(temp.izq, busqueda.dato);
			}
			return true;
		}
		else
			return false;		//El dato a borrar no est� en el �rbol
	}
	
	//Elimina todos los datos del �rbol al poner la raiz como nula
	public void vaciar(){
		raiz = null;
	}
	
	//Simplifica al m�todo principal de b�squeda
	public Nodo buscar(int b){
		return buscar(raiz, b);
	}
	
	//M�todo que regresa el Nodo que posea el dato b o null en caso de no encontrarlo
	public Nodo buscar(Nodo n, int b){
		while(n!=null){
			if(n.dato==b)
				return n;
			else if(n.dato>b)
				n=n.izq;
			else
				n=n.der;
		}
		return null;
	}

	//Este m�todo hace una rotaci�n sencilla a la izquierda con un nodo (p) y su hijo derecho (rc)
	public void rotacionIzq(Nodo p, Nodo rc){		
		int w;
		Nodo temp = rc.izq;
		if(p==raiz) {
			raiz = rc;
			rc.padre=null;
		}
		else if(p.dato>p.padre.dato) {
			p.padre.der = rc;
			rc.padre = p.padre;
		}
		else {
			p.padre.izq = rc;
			rc.padre = p.padre;
		}
		
		rc.izq = p;
		p.der = temp;
		p.padre = rc;
		if(temp!=null)
			temp.padre = p;
		
		w = p.coef;
		p.coef=(w-1)-Math.max(rc.coef,0);

		rc.coef = Math.min(Math.min((w-2),(w + rc.coef-2)),(rc.coef-1));
	}

	//Rotaci�n sencilla a la derecha con el nodo p y su hijo izquierdo (lc)
	public void rotacionDer(Nodo p, Nodo lc) {
		int w;
		Nodo temp = lc.der;
		if(p==raiz) {
			raiz=lc;
			lc.padre=null;
		}
		else if(p.dato>p.padre.dato) {
			p.padre.der = lc;
			lc.padre = p.padre;
		}
		else {
			p.padre.izq = lc;
			lc.padre = p.padre;
		}
		
		lc.der = p;
		p.izq = temp;
		p.padre = lc;
		if(temp!=null)
			temp.padre = p;
		
		w = p.coef;
		p.coef=(w+1)-Math.min(lc.coef,0);

		lc.coef=Math.max(Math.min((w+2),(w-lc.coef+2)),(lc.coef+1));
	}
	
	//Este m�todo se encarga de reajustar los balances en el �rbol a partir del nodo especificado en caso de que sea agregado un nuevo nodo
	//adem�s de revisar si es necesario hacer alguna rotaci�n
	public void balanceAdd(Nodo nodo, int incremento) {	
		int tR=-1;
		while(nodo!=null) {
			nodo.coef = nodo.coef + incremento; //Reajusta balances
			if(nodo.coef==0) //Si es igual a 0 se detiene
				return;
			else {
				tR=tipoRotacion(nodo); //llamamos al metodo tipoRotacion
				
				switch(tR){
				case 0: //No hay necesidad de rotar
					if(nodo.padre!=null){
						if(nodo.dato<nodo.padre.dato)
							incremento=-1;
						else
							incremento=1;
					}
					nodo=nodo.padre;
					break;
				case 1:	//Rotacion sencilla izquierda
					rotacionIzq(nodo,nodo.der);
					return;
				case 2:	//Rotacion doble izquierda
					rotacionDer(nodo.der,nodo.der.izq);
					rotacionIzq(nodo,nodo.der);
					return;
				case 3:	//Rotacion sencilla derecha
					rotacionDer(nodo,nodo.izq);
					return;
				case 4:	//Rotacion doble derecha
					rotacionIzq(nodo.izq, nodo.izq.der);
					rotacionDer(nodo, nodo.izq);
					return;
				}
			}
		}
	}

	//Es encargado de reajustar los balances y checar rotaciones a partir del nodo dado en caso de que un dato sea eliminado
	public void balanceDel(Nodo nodo, int incremento) {	
		int tR=-1;
		Nodo temp = nodo;

		while(nodo!=null) {
			nodo.coef = nodo.coef - incremento;
			if(nodo.coef==-1 || nodo.coef==1)
				return;
			else{
				//Pregunta al m�todo tipoRotacion que tipo de rotacion se debe usar si es aplicable
				tR = tipoRotacion(nodo);
				
				switch(tR){
				case 0:	//En este caso no es necesario hacer una rotaci�n
					if(nodo.padre!=null){
						if(nodo.dato<nodo.padre.dato)
							incremento=-1;
						else
							incremento=1;
					}
					nodo=nodo.padre;
					break;
				case 1:	//Rotacion sencilla a la izquierda
					temp = nodo.der;
					rotacionIzq(nodo, temp);
					break;
				case 2:	//Rotacion doble izquierda
					temp = nodo.der.izq;
					rotacionDer(nodo.der, temp);
					rotacionIzq(nodo, nodo.der);
					break;
				case 3:	//Rotacion sencilla derecha
					temp = nodo.izq;
					rotacionDer(nodo, temp);
					break;
				case 4:	//Rotacion doble derecha
					temp = nodo.izq.der;
					rotacionIzq(nodo.izq, temp);
					rotacionDer(nodo, nodo.izq);
					break;
				}
				
				if(tR != 0){		//Checa si se debe seguir hacia arriba despu�s de haber rotado
					if(temp.coef==-1||temp.coef==1)
						return;
					else{
						if(temp.padre!=null) {
							if(temp.dato<temp.padre.dato)
								incremento=-1;
							else
								incremento=1;
						}
						nodo=temp.padre;
					}
				}
			}
		}
	}
	
	//Este m�todo checa el tipo de rotaci�n que ser�a necesario ocupar en base al coeficiente de balance de nodo
	public int tipoRotacion(Nodo p) {
		if(p.coef==2) {
			if((p.der.coef==0)||(p.der.coef==1))
				return 1;
			else if(p.der.coef==-1)
				return 2;
		}
		else if(p.coef==-2) {
			if((p.izq.coef==0)||(p.izq.coef==-1))
				return 3;
			else if(p.izq.coef==1)
				return 4;
		}
		return 0;
	}
	
	//Este m�todo genera una nueva imagen del arbol como est� ahora y la devuelve
	public ArbolGrafico desplegar(){
		return new ArbolGrafico(this);
	}
	
	/*//M�todo para imprimir el arbol en linea de comandos o debugging
	public void despliega(Nodo n, int nivel){
		if(n!=null){
			despliega(n.der, nivel+1);
			
			for(int i=0; i<nivel; i++)
				System.out.print("-----");
			System.out.println(n.dato + "[" + n.coef + "]");
			
			despliega(n.izq, nivel+1);
		}
	}*/
}
