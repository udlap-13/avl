//Interfaz con la cual el usuario va a interactuar
import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;

public class Interfaz extends AbstractAction{
	private JFrame f;
	private JPanel dibujo;
	private ArbolAVL a;
	
	private JTextField field;
	private JTextArea area;
	
	//Constructor de la interfaz que se encarga crear la ventana e inicializar variables
	public Interfaz(){
		f = new JFrame("�rbol AVL");
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.setExtendedState(JFrame.MAXIMIZED_BOTH);
		f.setLayout(new BorderLayout());
		
		a = new ArbolAVL();
		
		dibujo = new JPanel();
		dibujo.setLayout(new BorderLayout());
		
		setToolBar();
		setLogArea();
		
		f.add(dibujo, BorderLayout.CENTER);
		
		f.pack();
		f.setVisible(true);
	}
	
	//Este metodo se encarga de inicializar la barra me opciones que se encuentra en la parte superior de la ventana
	private void setToolBar(){
		JToolBar tb = new JToolBar();
		
		field = new JTextField(7);
		field.setMaximumSize(field.getPreferredSize());
		field.setActionCommand("field");
		field.addActionListener(this);
		
		JButton insert = new JButton("Agregar");
		JButton delete = new JButton("Borrar");
		JButton find = new JButton("Buscar");
		JButton clear = new JButton("Vaciar");
		JButton exit = new JButton("Salir");
		
		insert.setActionCommand("insert");
		insert.addActionListener(this);
		delete.setActionCommand("del");
		delete.addActionListener(this);
		find.setActionCommand("find");
		find.addActionListener(this);
		clear.setActionCommand("clear");
		clear.addActionListener(this);
		exit.setActionCommand("exit");
		exit.addActionListener(this);
		
		tb.addSeparator();
		tb.add(field);
		tb.addSeparator();
		tb.add(insert);
		tb.add(delete);
		tb.add(find);
		tb.addSeparator();
		tb.add(clear);
		
		tb.add(Box.createHorizontalGlue());
		tb.add(exit);
		
		
		tb.setFloatable(false);
		
		f.add(tb, BorderLayout.PAGE_START);
	}
	
	//Se encarga de crear el area de texto que se encuantra en la parte inferior de la ventana
	//y en el cual se montraran todos los mensajes de acciones realizadas por el usuario
	private void setLogArea(){
		area = new JTextArea();
		area.setEditable(false);
		
		JScrollPane scroll = new JScrollPane (area);
		scroll.setPreferredSize(new Dimension(10, 177));
		
		f.add(scroll, BorderLayout.PAGE_END);
	}
	
	//Este metodo se encarga de generar un nuevo grafico del arbol usando la clase ArbolGrafico
	//Ademas de actualizar el JPanel que contine el grafico con el nuvo grafico
	public void repintarArbol(){
		dibujo.removeAll();
		dibujo.add(a.desplegar(), BorderLayout.CENTER);
		
		
		
		dibujo.revalidate();
        dibujo.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Se ejecuta cuando se selecciona el boton de insertar o cuando se aprieta enter en el cmpo de texto
		//Esta encargado de insertar los datos que se pongan en el campo de texto
		if("insert".equals(e.getActionCommand()) || "field".equals(e.getActionCommand())){
			area.setText("");
			String s = field.getText();
			if(!"".equals(s)){
				String[] ar = s.replaceAll("^[,\\s]+", "").split("[,\\s]+");		//Permite leer m�tiples valores separados por espacios o comas
				
				area.append("Agregando...");
				for(int i=0; i<ar.length; i++){
					if(a.agregar(Integer.parseInt(ar[i])))
						area.append("\n" + Integer.parseInt(ar[i]) +": Correctamente agregado!");
					else
						area.append("\n" + Integer.parseInt(ar[i]) +": Se encuentra repetido");
				}

				repintarArbol();
				field.setText("");
			}
		}
		//Se ejecuta al ser presionado el boton de borrar, y se encarga de borrar lso datos especificados en el campo de texto
		else if("del".equals(e.getActionCommand())){
			area.setText("");
			String s = field.getText();
			if(!"".equals(s)){
				String[] ar = s.replaceAll("^[,\\s]+", "").split("[,\\s]+");
				
				area.append("Eliminando..");
				for(int i=0; i<ar.length; i++){
					if(a.eliminar(Integer.parseInt(ar[i])))
						area.append("\n" + Integer.parseInt(ar[i]) +": Correctamente eliminado!");
					else
						area.append("\n" + Integer.parseInt(ar[i]) +": No se encuentra en el �rbol");
				}

				repintarArbol();
				field.setText("");
			}
		}
		//Se ejecuta al presionar el boton de buscar, y se encarga de buscar el primer valor que sea puesto en el campo de texto
		else if("find".equals(e.getActionCommand())){
			area.setText("");
			String s = field.getText();
			Nodo n = null;
			if(!"".equals(s)){
				String[] ar = s.replaceAll("^[,\\s]+", "").split("[,\\s]+");
				
				n = a.buscar(Integer.parseInt(ar[0]));
				area.append("Buscando " + Integer.parseInt(ar[0]) + "...\n");
				if(n!=null)
					area.append("Dato encontrado: " + n.dato + "[" + n.coef + "]");
				else
					area.append("Dato no encontrado");
				
				field.setText("");
			}
		}
		//Se ejecuta al presionar el boto�n de vaciar, y se encarga de vaciar el �rbol
		else if("clear".equals(e.getActionCommand())){
			area.setText("");
			a.vaciar();
			
			area.append("El �rbol ha sido vaciado!");
			repintarArbol();
		}
		
		else if("exit".equals(e.getActionCommand())){
			System.exit(0);
		}
	}
	
	
	public static void main(String[] args){
		new Interfaz();
	}
}
