//Clase Nodo. Cada objeto que sea creado de esta clase representa un nodo dentro del �rbol
public class Nodo {
	int dato;
	int coef;
	Nodo izq, der, padre;
	
	public Nodo(int x, Nodo p){
		dato = x;
		coef = 0;
		izq = der = null;
		padre = p;
	}
}
